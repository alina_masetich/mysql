use univercity;

# №1.1
SELECT * FROM Prepods;

# №1.2
SELECT * FROM Students
WHERE Group_id = (SELECT id FROM _Groups WHERE Name='123456')
ORDER BY Last_name;

# №1.3
SELECT * FROM Students
WHERE Last_name LIKE 'O%';

# №2.1
SELECT _Date, Val FROM Rating
WHERE Prepod_id = 
(SELECT id FROM Prepods WHERE Last_name='Provolotskiy') 
AND _Date LIKE '2016-10-%';

# №2.2
SELECT 
(SELECT Last_name From Students WHERE Id = Student_id) as Student
FROM Rating
WHERE Is_absent = 'Y' 
AND Study_id IN 
(SELECT Id FROM Study_load WHERE Type_study_id = 
(SELECT Id FROM Type_study_load WHERE Name='LK'));

# №2.3
SELECT Val,
(SELECT Name FROM _Groups WHERE Id = 
(SELECT Group_id FROM Students WHERE Id = Student_id)) as Group_,
(SELECT Name FROM Faculty WHERE Id = 
(SELECT Faculty_id FROM _Groups WHERE Id = 
(SELECT Group_id FROM Students WHERE Id = Student_id))) as Faculty,
(SELECT Last_name FROM Students WHERE Id = Student_id) as Student,
(SELECT Last_name FROM Prepods WHERE Id = Prepod_id) as Prepod,
_Date FROM Rating 
WHERE VAL IS NOT NULL;

# №2.4
SELECT * FROM Rating
WHERE Prepod_id NOT IN 
(SELECT Prepod_id FROM Lessons);

# №2.5
SELECT
DISTINCT(SELECT Last_name From Prepods WHERE Id = Prepod_id) as Prepod,
(SELECT Name From _Groups WHERE Id =
(SELECT Group_id From Students WHERE Id = Student_id)) as _Group
FROM Rating
ORDER BY Prepod;

# №2.6
SELECT distinct prepods.last_name, _Groups.Name
FROM prepods
LEFT JOIN rating on rating.Prepod_id = prepods.id
LEFT JOIN students on students.id = rating.student_id
LEFT JOIN _Groups on _Groups.id = students.group_id
ORDER BY prepods.last_name desc;

# 2.7
SELECT Id, Name FROM Faculty
WHERE Id NOT IN (SELECT Faculty_id From _Groups);















