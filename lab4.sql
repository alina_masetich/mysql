use univercity;

INSERT Faculty(Name) 
VALUES 
	('Ksis'),
    ('Fitu');
    
INSERT _Groups(Name, Faculty_id)
VALUES ('123456', 1);

INSERT Students(Last_name, Group_id)
VALUES 
    ('Petrov', 1),
    ('Student1', 1),
    ('Ivanov', (select id from _Groups where Name='123456')),
    ('Student2', 1),
    ('Student3', 1),
    ('Student4', 1),
    ('Student3', 1),
    ('O1', 1),
    ('O2', 1),
    ('Student5', 1);
    
UPDATE _Groups
SET Head_group = 1
WHERE Id = 1;

INSERT Prepods(Last_name, Faculty_id)
VALUES
	('Vasechkin', 1),
    ('Petechkin', 2),
    ('Provolotskiy', 1),
    ('Prepod', 1);
    
INSERT Subjects(Name, Faculty_id)
VALUES 
	('Vvedenie v spets', 1),
    ('Subject1', 1),
    ('Subject2', 1),
    ('Subject3', 1),
    ('Subject4', 1);

INSERT Type_study_load(Name)
VALUES 
	('LK'),
    ('PZ'),
    ('LR');

INSERT Study_load(Id, Subject_id, Type_study_id)
VALUES 
	('VVS', 1, 1),
    ('Sub1', 2, 1),
    ('Sub2', 3, 2),
    ('Sub3', 4, 2),
    ('Sub4', 5, 3);

INSERT Lessons(Group_id, Prepod_id, Study_id)
VALUES 
	(1, 1, 'VVS'),
    (1, 1, 'Sub1'),
    (1, 1, 'Sub2'),
    (1, 2, 'Sub3'),
    (1, 2, 'Sub4');

INSERT Rating(_Date, Val, Student_id, Study_id, Prepod_id, Is_absent)
VALUES
	("2018-10-01", NULL, 1, 'VVS', 1, default),
    ("2018-10-01", NULL, 2, 'VVS', 1, 'Y'),
    
    ("2018-09-01", 10, 3, 'Sub1', 1, DEFAULT),
    ("2018-11-01", 9, 3, 'Sub3', 1, DEFAULT),
    ("2016-10-11", 5, 3, 'Sub4', 3, DEFAULT),
	
    ("2018-11-01", 10, 4, 'Sub2', 1, DEFAULT),
    ("2018-11-01", 8, 4, 'Sub1', 1, DEFAULT),
    ("2018-11-01", 9, 4, 'Sub4', 2, DEFAULT),
    
    ("2016-10-18", 8, 5, 'Sub2', 3, DEFAULT),
    ("2018-11-01", 8, 5, 'Sub1', 1, DEFAULT),
    ("2018-03-01", 9, 5, 'Sub4', 2, DEFAULT),
    
    ("2018-11-01", 7, 6, 'Sub3', 1, DEFAULT),
    ("2016-10-25", 10, 6, 'Sub1', 3, DEFAULT),
    ("2018-11-01", 4, 6, 'Sub4', 2, DEFAULT),
    
    ("2018-08-01", 6, 7, 'Sub2', 1, DEFAULT),
    ("2018-11-01", 10, 7, 'Sub3', 1, DEFAULT),
    ("2018-11-01", 9, 7, 'Sub4', 2, DEFAULT);

#drop database if exists univercity;