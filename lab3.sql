drop database if exists univercity;

CREATE DATABASE univercity ;

USE univercity;

CREATE TABLE IF NOT EXISTS Faculty(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Name VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS _Groups(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Name VARCHAR(10) NOT NULL,
    Head_group INT,
    Faculty_id INT NOT NULL,
    FOREIGN KEY(Faculty_id) REFERENCES Faculty(Id)
);

 CREATE TABLE IF NOT EXISTS Students(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Last_name VARCHAR(60) NOT NULL,
    First_name VARCHAR(30),
    Group_id INT,
    FOREIGN KEY(Group_id) REFERENCES _Groups(Id)
 );
 
 ALTER TABLE _Groups 
 ADD FOREIGN KEY(Head_group) REFERENCES Students(Id);
 
 CREATE TABLE IF NOT EXISTS Prepods(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Last_name VARCHAR(120) NOT NULL,
    First_name VARCHAR(120),
    Faculty_id INT NOT NULL,
    FOREIGN KEY(Faculty_id) REFERENCES Faculty(Id)
 );
 
 CREATE TABLE IF NOT EXISTS Subjects(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Name VARCHAR(120) NOT NULL,
    Faculty_id INT,
	FOREIGN KEY(Faculty_id) REFERENCES Faculty(Id)
 );
 
 CREATE TABLE IF NOT EXISTS Type_study_load(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Name VARCHAR(120) NOT NULL UNIQUE
 );
 
 CREATE TABLE IF NOT EXISTS Study_load(
	Id CHAR(20) PRIMARY KEY NOT NULL,
	Hours INT,
    Subject_id INT NOT NULL,
    Type_study_id INT NOT NULL,
    FOREIGN KEY(Subject_id) REFERENCES Subjects(Id),
    FOREIGN KEY(Type_study_id) REFERENCES Type_study_load(Id)
 );
 
 CREATE TABLE IF NOT EXISTS Lessons(
	Group_id INT NOT NULL,
    Prepod_id INT NOT NULL,
    Study_id CHAR(20) NOT NULL,
	PRIMARY KEY(Group_id, Prepod_id, Study_id),
    FOREIGN KEY(Group_id) REFERENCES _Groups(Id),
    FOREIGN KEY(Prepod_id) REFERENCES Prepods(Id),
    FOREIGN KEY(Study_id) REFERENCES Study_load(Id)
 );
 
 CREATE TABLE IF NOT EXISTS Rating(
	Id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    Date DATE NOT NULL,
    Val INT,
    Student_id INT NOT NULL,
    Study_id CHAR(20) NOT NULL,
    Prepod_id INT NOT NULL,
    Is_absent CHAR(1) DEFAULT 'N',
    FOREIGN KEY(Student_id) REFERENCES Students(Id),
    FOREIGN KEY(Study_id) REFERENCES Study_load(Id),
    FOREIGN KEY(Prepod_id) REFERENCES Prepods(Id)
 );