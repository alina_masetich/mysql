use univercity;

# №1.1
SELECT COUNT(*) AS LessonsCount
FROM Rating;

# №1.2
SELECT Students.last_name as Student,
count(*) k, sum(Val) s, avg(Val) _avg
FROM Rating 
INNER JOIN Students on Student_id = Students.id
GROUP BY Student_id
ORDER BY avg(Val) DESC
LIMIT 5;

# №1.3
SELECT count(distinct Study_id) as study, count(distinct(Type_study_load.Id)) AS count_type_less 
FROM Rating
LEFT JOIN Study_load on Study_id = Study_load.Id
LEFT JOIN Type_study_load on Type_study_id = Type_study_load.Id
WHERE Val is not null;

# №1.4
SELECT Prepods.last_name as Prepod, 
count(*)
FROM Rating
left JOIN Prepods on Prepod_id = Prepods.id
group by Prepod_id
order by count(*) ;

# №1.5
SELECT distinct Prepods.last_name, count(val) as count_of_marks
FROM Prepods
LEFT JOIN Rating on Rating.Prepod_id = Prepods.id
GROUP BY Prepod_id
ORDER BY count_of_marks asc;

# №1.6
SELECT Students.last_name as Student,
count(*) k, sum(Val) s, avg(Val) _avg
FROM Rating 
INNER JOIN Students on Rating.student_id = Students.id
GROUP BY Student_id
ORDER BY avg(Val) DESC
LIMIT 1;

